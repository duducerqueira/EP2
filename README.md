# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Programa desenvolvido na IDE "Netbeans"

Como excecutar o programa:

1-Selecione a Classe main e rode o programa usando a IDE de sua escolha(netbeans ou eclipse);
2-Na tela de Login, para passar pelo login digite "aaa" no usuario e "123" na senha;
3-Selecione a opção "Cadastrar produto no estoque";
4-Selecione o tipo de alimento desejado, preencha os campose clique em "Criar";
5-Após criar o estoque, selecione a opção "Criar novo pedido";
6-Selecione no cardápio o prato desejado, preencha os outros campos e clique em "Processar pedido;
7-Se desejar selecionar mais desejar adicionar mais um alimento ao seu pedido clique em "sim" e repita o passo 6 , se não clique em "não" se quiser cancelar clique em "cancelar";
8-Selecione a forma de pagamento;
9-Essa é a conta do seu pedido!

package model;

import java.util.ArrayList;

public class Stock {
    
    public static ArrayList <Product> stockProducts = new ArrayList();
    

    public static ArrayList getProducts() {
        return stockProducts;
    }

    public void setProducts(ArrayList <Product> products) {
        this.stockProducts = products;
    }
    
    public static void addDessertToStock(Dessert dessert){
	stockProducts.add(dessert);
    }
    
    public static void addDishToStock(Dish dish){
	stockProducts.add(dish);
    }
    
    public static void addDrinkToStock(Drink drink){
	stockProducts.add(drink);
    }
    
    public static void addProductToStock(Product product){
	stockProducts.add(product);
    }
    
}

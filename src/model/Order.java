package model;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import static model.Stock.stockProducts;
import model.Product;

public class Order {

    private static double totalValue;
    private static ArrayList <Product> orderedProducts = new ArrayList();
    private String paymentType;
    private String observation;


    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public static ArrayList <Product>  getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(ArrayList <Product>  orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public static void addProductToOrder(Product product){
	orderedProducts.add(product);
    }
    
    public void processTotalValue (){
        for (int aux = 0; aux < orderedProducts.size(); aux++){
            this.totalValue += orderedProducts.get(aux).getPrice();
        }
    }

    /**
     * @return the observacoes
     */
    public String getObservation() {
        return observation;
    }

    /**
     * @param observation the observacoes to set
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }
    
}

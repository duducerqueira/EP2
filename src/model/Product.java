package model;

public class Product {
    
    private String name;
    private Integer stockAmount;
    private double price;
    
    Product(String name, int stockAmount, double price){
        setName(name);
        setStockAmount(stockAmount);
        setPrice(price);
     
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(Integer amount) {
        this.stockAmount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
   
    
   
}

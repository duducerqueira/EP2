package model;

import java.util.Scanner;

public class Employee extends Person{
    
    private String password;
    private static String login;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
